package com.miningmark48.pearcelmod.block;

import net.minecraft.block.BlockPane;
import net.minecraft.block.material.Material;

public class BlockPearcelPane extends BlockPane {

    public BlockPearcelPane(String p_i45432_1_, String p_i45432_2_, Material p_i45432_3_, boolean p_i45432_4_) {
        super(p_i45432_1_, p_i45432_2_, p_i45432_3_, p_i45432_4_);
        this.setHardness(2.0F);
        this.setResistance(1.5F);
    }

}
